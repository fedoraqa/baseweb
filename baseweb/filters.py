
def tagify(value):
    return value.replace(' ', '')


def urlsplit(value):
    return value.split('/')


def getname(value, entry=1):
    return value.split('/')[entry]


def getsubname(value):
    urlparts = value.split('/')
    if len(urlparts) >= 5:
        return ''.join(urlparts[2:4])
    else:
        return ''


def updatetype(update):
    is_fe = False
    for bug in update.bugs:
        if bug.proposed_blocker or bug.accepted_blocker:
            return 'blocker'
        elif bug.proposed_fe or bug.accepted_fe:
            is_fe = True
    if is_fe:
        return 'FE'


def updatelabel(bug):
    label = []
    lastupdate = bug.updates.first()
    if lastupdate:
        if lastupdate.status == 'stable':
            if bug.status in ['MODIFIED', 'ON_QA', 'VERIFIED', 'CLOSED']:
                label.append('<span class="radius success label">')
                if lastupdate.pending:
                    label.append('pending ')
                label.append('stable')

        elif lastupdate.status == 'testing':
            label.append('<span class="radius alert label">')
            if lastupdate.pending:
                label.append('pending ')
            label.append('testing')

    return ''.join(label)

def datetime_format(value, format='%Y-%m-%d %H:%M:%S UTC'):
    if value is not None:
        return value.strftime(format)
    return ''


filters = {'tagify':tagify, 'urlsplit':urlsplit, 'getname':getname, 'getsubname':getsubname}
