.. image:: http://unmaintained.tech/badge.svg
  :target: http://unmaintained.tech/
  :alt: No Maintenance Intended

.. important::
  **THIS PROJECT IS NOW UNMAINTAINED.**

  We don't need this project anymore, so we don't develop it nor maintain it.


Fedora QA Base Web Elements
===========================

This repository is the common web elements shared between the QA web apps.


Dependencies
------------

Both `compass <http://compass-style.org/>`_ and `SASS <http://sass-lang.com/>`_
are required to build the css. To install on recent Fedora (not available on el6)::
  yum install rubygem-compass rubygem-sass

Building CSS
------------

To build the CSS::
  make css

Build CSS on Change
-------------------

To build the CSS on change to the scss templates::
  make watch

Serve baseweb directory over port 8000
--------------------------------------

Most web browsers don't allow locally imported fonts, so serving the example
index.html with python's SimpleHTTPServer makes development easier::
  make serve
