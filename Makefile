BASEDIR=$(CURDIR)
OUTBASE=$(BASEDIR)/baseweb

css:
	compass compile

watch:
	compass watch

serve:
	cd $(OUTBASE) && python -m SimpleHTTPServer
